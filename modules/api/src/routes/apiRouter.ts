import { Args, HttpBadRequestError, TraceUtils } from '@themost/common';
import { Router } from 'express';

export function apiRouter(passport: any): Router {
    const router = Router();
    router.get('/messages', async (req, res, next) => {
        try {
            const items = await req.context.model('SmsMessages')
                .orderByDescending('dateReceived')
                .silent()
                .getItems();
            return res.json({
                value: items
            });
        } catch (error) {
            return next(error);
        }
    });
    router.post('/messages/send', passport.authenticate('basic', { session: false }), async (req, res, next) => {
        try {
            const body = Object.assign({}, req.body);
            Args.check(Array.isArray(body) === false, new HttpBadRequestError('Expected object'));
            await req.context.model('SmsMessage').silent().save(body);
            return res.json(body);
        } catch (error) {
            return next(error);
        }
    });
    router.post('/messages/balance', (req, res, next) => {
        return res.json({
            balance: 1000
        });
    });
    router.get('/messages/:identifier', async (req, res, next) => {
        try {
            const item = await req.context.model('SmsMessages')
                .where('identifier').equal(req.params.identifier)
                .silent()
                .getItem();
            return res.json({
                value: item
            });
        } catch (error) {
            return next(error);
        }
    });
    return router;
}
