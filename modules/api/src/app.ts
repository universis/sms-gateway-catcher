import * as express from 'express';
import * as cors from 'cors';
import {ExpressDataApplication} from '@themost/express';
import { resolve } from 'path';
import { apiRouter } from './routes/apiRouter';
const engine = require('ejs-locals');
import * as passport from 'passport';
import { BasicStrategy } from 'passport-http'
import { HttpUnauthorizedError } from '@themost/common';
const app = express();
app.use(cors({
    origin:true, 
    credentials: true
}));

// use ejs-locals for all ejs templates
app.engine('ejs', engine);
// view engine setup
app.set('views', resolve(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(express.json());
app.use(express.urlencoded({ extended: false }));

const dataApplication = new ExpressDataApplication(resolve(__dirname, 'config'));
app.set(ExpressDataApplication.name, dataApplication);

passport.use(new BasicStrategy(
    {
        passReqToCallback: true
    },
    function(req, username, password, done) {
      const authorization: {user: string, password: string} = req.context.getApplication().getConfiguration().getSourceAt('settings/auth');
      if (authorization) {
        if (username === authorization.user && password === authorization.password) {
            return done(null, {
                user: {
                    name: authorization.user
                }
            })
        }
      }
      return done(new HttpUnauthorizedError());
    }
  ));

app.use(dataApplication.middleware(app));

app.use('/api', apiRouter(passport));

export = app;
