# @universis/sms-gateway-catcher

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 11.2.14.

## Installation

Install dependencies by executing `npm ci`. You should also install dependencies of api module. Navigate to folder `modules/api` and execute `npm ci` also.

## Development server

Run api module of sms gateway catcher `cd modules/api && npm run serve` and finally

run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Build

Run `npm run build`

## Production

Run `npm run start:production` to start sms gateway catcher in production mode.