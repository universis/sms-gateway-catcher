import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MessageService } from './message.service';
import { MessageListComponent } from './message-list/message-list.component';
import { MostModule } from '@themost/angular';
import { LinkifyPipe } from './linkify.pipe';

@NgModule({
  declarations: [
    AppComponent,
    MessageListComponent,
    LinkifyPipe
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    MostModule.forRoot({
      base: '/api/',
      options: {
        useMediaTypeExtensions: false,
        useResponseConversion: true
      }
    })
  ],
  providers: [
    MessageService,
    LinkifyPipe
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
