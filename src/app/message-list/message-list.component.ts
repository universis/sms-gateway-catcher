import { Component, ElementRef, OnDestroy, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { Subscription } from 'rxjs';
import { webSocket } from "rxjs/webSocket";
import { MessageService } from '../message.service';

@Component({
  selector: 'app-message-list',
  templateUrl: './message-list.component.html',
  styleUrls: ['./message-list.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class MessageListComponent implements OnInit, OnDestroy {

  text = '';
  messages: Array<any> = [];
  subscription:Subscription;
  @ViewChild('messageHistory') messageHistory: ElementRef;

  constructor(private messageService: MessageService) {
      this.subscription = this.messageService.messenger.subscribe( value => {
          if (value) {
            this.messages.push(value);
            setTimeout(()=> {
              this.messageHistory.nativeElement.scrollTop = this.messageHistory.nativeElement.scrollHeight;
            }, 100);
          }
      });
  }

  async ngOnInit() {
    // get messages
    this.messages = await this.messageService.getMessages();
    setTimeout(()=> {
              this.messageHistory.nativeElement.scrollTop = this.messageHistory.nativeElement.scrollHeight;
            }, 100);
  }

  ngOnDestroy() {
    // prevent memory leak when component is destroyed
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }

  send(sender, recipient, text) {
    this.messageService.sendMessage(sender, recipient, text);
  }

  onEnter(event) {
    if (event.keyCode === 13) {
      event.preventDefault;
      const text = event.target.value;
      this.messageService.sendMessage('Olia', 'Vincent', event.target.value);
      event.target.value = '';
      return false;
    }
  }


}
