import { Injectable } from '@angular/core';
import { AngularDataContext } from '@themost/angular';
import { BehaviorSubject } from 'rxjs';
import { LinkifyPipe } from './linkify.pipe';

@Injectable()
export class MessageService {

  constructor(private context: AngularDataContext, private linkify: LinkifyPipe) {

  }

  private _messengerSource = new BehaviorSubject<any>(null);
  // Observable stream
  public messenger = this._messengerSource.asObservable();

  async getMessages() {
      return this.context.model('messages').asQueryable().getItems().then((items)=> {
        const results: Array<any> = items.map((item) => {
          return Object.assign(item, {
            text: this.linkify.transform(item.text)
          })
        });
        return results;
      });
  }

  async sendMessage(sender, recipient, text) {
    const newMessage = {
          "sender": sender,
          "recipient": recipient,
          "text": text,
          "dateSent": new Date(),
          "dateRead": null
        }
  }


}